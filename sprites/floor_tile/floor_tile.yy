{
    "id": "272e329d-1c3a-4118-b5cb-f0c741e74d43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "floor_tile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "032f782c-f071-4365-a117-f07dc8ba1d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272e329d-1c3a-4118-b5cb-f0c741e74d43",
            "compositeImage": {
                "id": "c5199190-4daf-4911-83f1-27575fd20ba9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "032f782c-f071-4365-a117-f07dc8ba1d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a516b0-d2ab-4cea-b65a-41e1754fcc09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "032f782c-f071-4365-a117-f07dc8ba1d7c",
                    "LayerId": "d22ee0ef-337a-4b3d-8ec4-6ff1c20a8b18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d22ee0ef-337a-4b3d-8ec4-6ff1c20a8b18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "272e329d-1c3a-4118-b5cb-f0c741e74d43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}