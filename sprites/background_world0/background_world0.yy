{
    "id": "4bd8210b-0bfd-4a56-a11a-2d52b6330606",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_world0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b72274bb-f988-4f7c-9704-19b5e5a6d79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4bd8210b-0bfd-4a56-a11a-2d52b6330606",
            "compositeImage": {
                "id": "3f6c3af4-abe7-4be9-9bc4-c41421c4dfa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72274bb-f988-4f7c-9704-19b5e5a6d79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b2d8621-b842-41fb-a374-590269576b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72274bb-f988-4f7c-9704-19b5e5a6d79b",
                    "LayerId": "99609899-54ce-443a-811c-b8ffac09e357"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "99609899-54ce-443a-811c-b8ffac09e357",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4bd8210b-0bfd-4a56-a11a-2d52b6330606",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "31baa7f3-12f4-44cf-b6c6-9c601706d0d7",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}