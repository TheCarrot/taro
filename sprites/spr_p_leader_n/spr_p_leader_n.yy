{
    "id": "30b1e90b-2468-4a7c-b810-86ed144d1a59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_leader_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77cfaa6b-212e-4fd5-a7fc-a3337aeba2bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b1e90b-2468-4a7c-b810-86ed144d1a59",
            "compositeImage": {
                "id": "22a9ceec-9f80-4fa5-b2e3-1e8b5f0db28b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77cfaa6b-212e-4fd5-a7fc-a3337aeba2bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ce10661-934c-48da-9238-c89ac663fe4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77cfaa6b-212e-4fd5-a7fc-a3337aeba2bd",
                    "LayerId": "625f6f6f-cdf1-4428-8d37-ec38bcd24250"
                }
            ]
        },
        {
            "id": "f5be83b0-8a35-4211-8bf6-049cac3a58bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30b1e90b-2468-4a7c-b810-86ed144d1a59",
            "compositeImage": {
                "id": "52dd0c82-cf31-4958-a300-3df078a65f44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5be83b0-8a35-4211-8bf6-049cac3a58bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60516038-52ec-483c-9c82-e9acb25d83ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5be83b0-8a35-4211-8bf6-049cac3a58bf",
                    "LayerId": "625f6f6f-cdf1-4428-8d37-ec38bcd24250"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "625f6f6f-cdf1-4428-8d37-ec38bcd24250",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30b1e90b-2468-4a7c-b810-86ed144d1a59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}