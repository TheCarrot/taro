{
    "id": "e677bb44-a328-4451-a88a-bc446d9c8ece",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_leader_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c2f686c-4899-4bbc-b87f-f17020a37c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e677bb44-a328-4451-a88a-bc446d9c8ece",
            "compositeImage": {
                "id": "bbed12d8-545f-4c30-a459-6ab34d8164e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c2f686c-4899-4bbc-b87f-f17020a37c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "501e1c3c-019b-4cce-9c42-092de774f25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c2f686c-4899-4bbc-b87f-f17020a37c68",
                    "LayerId": "557c5edc-5cd4-491a-8815-e025ab265aa7"
                }
            ]
        },
        {
            "id": "c10d7b1c-8221-431c-b063-c7acda2c2caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e677bb44-a328-4451-a88a-bc446d9c8ece",
            "compositeImage": {
                "id": "84f7fde3-2af5-4c1f-ac2b-3d1cef14828e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10d7b1c-8221-431c-b063-c7acda2c2caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cbd3941-9881-4b46-a9c4-084255630d65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10d7b1c-8221-431c-b063-c7acda2c2caa",
                    "LayerId": "557c5edc-5cd4-491a-8815-e025ab265aa7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "557c5edc-5cd4-491a-8815-e025ab265aa7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e677bb44-a328-4451-a88a-bc446d9c8ece",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}