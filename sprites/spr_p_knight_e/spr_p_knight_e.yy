{
    "id": "6606a051-836b-4d53-8e71-4ff309d82328",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_knight_e",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01095eef-9577-48b0-ac78-bc1f130e210c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6606a051-836b-4d53-8e71-4ff309d82328",
            "compositeImage": {
                "id": "49c9fdba-ceed-4f1b-9df2-20eac6db9b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01095eef-9577-48b0-ac78-bc1f130e210c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a7bbcf0-ff78-4b1f-af1f-49c25b9fa14e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01095eef-9577-48b0-ac78-bc1f130e210c",
                    "LayerId": "11290085-d73d-4c52-bc92-88be38e6ecdb"
                }
            ]
        },
        {
            "id": "f9f7003d-692f-46f4-a46e-1e5f34084463",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6606a051-836b-4d53-8e71-4ff309d82328",
            "compositeImage": {
                "id": "596603a8-c856-4fc3-8e2c-841946d9041e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f7003d-692f-46f4-a46e-1e5f34084463",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eedd34ec-fe28-4c74-8105-7a63d62562b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f7003d-692f-46f4-a46e-1e5f34084463",
                    "LayerId": "11290085-d73d-4c52-bc92-88be38e6ecdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "11290085-d73d-4c52-bc92-88be38e6ecdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6606a051-836b-4d53-8e71-4ff309d82328",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}