{
    "id": "3ecb1221-2270-479c-90a5-34e45ac546a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "world_tile_set",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67fe9abb-b49f-44d2-9d0f-0baae3511a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ecb1221-2270-479c-90a5-34e45ac546a7",
            "compositeImage": {
                "id": "2689c561-4c2c-4b8b-bffb-329b57f85cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67fe9abb-b49f-44d2-9d0f-0baae3511a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "207660e7-a1c2-40b1-a238-15958dff4269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67fe9abb-b49f-44d2-9d0f-0baae3511a8d",
                    "LayerId": "a207891a-176d-44d6-a392-0e4d3670b3cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 448,
    "layers": [
        {
            "id": "a207891a-176d-44d6-a392-0e4d3670b3cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ecb1221-2270-479c-90a5-34e45ac546a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 0,
    "yorig": 0
}