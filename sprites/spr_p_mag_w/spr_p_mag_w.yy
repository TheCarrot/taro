{
    "id": "2a5998e0-cb35-48bd-b516-60c45c89ee89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_mag_w",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 28,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c206e2e-951b-449f-af8c-c9d22833412c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5998e0-cb35-48bd-b516-60c45c89ee89",
            "compositeImage": {
                "id": "f53e7651-59b1-465b-a9a6-82a647321992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c206e2e-951b-449f-af8c-c9d22833412c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da8642f9-f2e7-4f80-9f2d-5397cb7a3711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c206e2e-951b-449f-af8c-c9d22833412c",
                    "LayerId": "041ffd35-050f-45f1-a83e-a714e478d4a5"
                }
            ]
        },
        {
            "id": "c586e78b-bb5c-4db7-9fec-9d5a530d76c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5998e0-cb35-48bd-b516-60c45c89ee89",
            "compositeImage": {
                "id": "ba67272a-d7b8-49ee-84be-8d929a5e4c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c586e78b-bb5c-4db7-9fec-9d5a530d76c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e61fa77c-93df-40d7-9afd-822a7b4453cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c586e78b-bb5c-4db7-9fec-9d5a530d76c2",
                    "LayerId": "041ffd35-050f-45f1-a83e-a714e478d4a5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "041ffd35-050f-45f1-a83e-a714e478d4a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a5998e0-cb35-48bd-b516-60c45c89ee89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}