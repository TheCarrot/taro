{
    "id": "56f35b52-4855-4dd0-a60c-e406e1731f9b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_mag_e",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2574af63-d1e8-4810-85ab-59916af31fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f35b52-4855-4dd0-a60c-e406e1731f9b",
            "compositeImage": {
                "id": "7d22c8fb-fca8-4539-8324-d17aa5c3f9db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2574af63-d1e8-4810-85ab-59916af31fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281fd95f-b6b0-43ac-b970-8ca265684360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2574af63-d1e8-4810-85ab-59916af31fa2",
                    "LayerId": "5b6e8344-3efa-41a3-a113-4cd17cd68831"
                }
            ]
        },
        {
            "id": "2e649402-2c2d-4aee-943e-fd8477daf85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56f35b52-4855-4dd0-a60c-e406e1731f9b",
            "compositeImage": {
                "id": "13462050-ff31-4bfe-bd82-6834811a2e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e649402-2c2d-4aee-943e-fd8477daf85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a781b671-74b9-4a3e-9fae-602094f37c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e649402-2c2d-4aee-943e-fd8477daf85e",
                    "LayerId": "5b6e8344-3efa-41a3-a113-4cd17cd68831"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5b6e8344-3efa-41a3-a113-4cd17cd68831",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56f35b52-4855-4dd0-a60c-e406e1731f9b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}