{
    "id": "6efb0b2f-a485-4c95-bba3-143964b3bb0e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "wall_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d013dc4-c91c-443c-b913-ea4b8bbf2162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6efb0b2f-a485-4c95-bba3-143964b3bb0e",
            "compositeImage": {
                "id": "f6f27cbc-b08d-4b31-90fb-03e1aa0ac751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d013dc4-c91c-443c-b913-ea4b8bbf2162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42200e56-e90c-464e-be99-3cc341194e20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d013dc4-c91c-443c-b913-ea4b8bbf2162",
                    "LayerId": "7c319984-cd3c-4a25-812b-cedfa77beaa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7c319984-cd3c-4a25-812b-cedfa77beaa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6efb0b2f-a485-4c95-bba3-143964b3bb0e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}