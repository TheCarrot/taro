{
    "id": "0592ed9a-76bf-4247-866b-70963827bb44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_priest_w",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbd15f03-f15c-462c-82e5-fd001385360a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0592ed9a-76bf-4247-866b-70963827bb44",
            "compositeImage": {
                "id": "ed02e069-076f-434b-bf1e-de1af2f09279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd15f03-f15c-462c-82e5-fd001385360a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37a4af79-f31f-4cf8-a967-89365a76cc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd15f03-f15c-462c-82e5-fd001385360a",
                    "LayerId": "0768cf26-f57d-4dd3-88b5-d54557916a10"
                }
            ]
        },
        {
            "id": "ecf86b62-44b3-494b-acee-6ae0f4dc226f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0592ed9a-76bf-4247-866b-70963827bb44",
            "compositeImage": {
                "id": "8cd26aad-9bbb-4fdd-9608-03b5e0533d9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecf86b62-44b3-494b-acee-6ae0f4dc226f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1836cc54-606a-483e-b9e2-6e1c8f1f1589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecf86b62-44b3-494b-acee-6ae0f4dc226f",
                    "LayerId": "0768cf26-f57d-4dd3-88b5-d54557916a10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0768cf26-f57d-4dd3-88b5-d54557916a10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0592ed9a-76bf-4247-866b-70963827bb44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}