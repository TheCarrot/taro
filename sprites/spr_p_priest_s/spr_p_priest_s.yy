{
    "id": "8d4e3c64-2d65-4468-9d9f-b9295b1c692c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_priest_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d669cd4b-3f1b-4722-a0e6-40162891765e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d4e3c64-2d65-4468-9d9f-b9295b1c692c",
            "compositeImage": {
                "id": "f1e720bd-76af-486d-be35-6daff37e55cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d669cd4b-3f1b-4722-a0e6-40162891765e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b883c816-41eb-49ca-9183-876a546873f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d669cd4b-3f1b-4722-a0e6-40162891765e",
                    "LayerId": "17b54054-d009-49dc-9cc6-acdc24c1b117"
                }
            ]
        },
        {
            "id": "8384e10a-bf62-4d62-9cb9-1b7230d23d5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8d4e3c64-2d65-4468-9d9f-b9295b1c692c",
            "compositeImage": {
                "id": "84208125-1573-4651-89e0-3ab2d1ce3ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8384e10a-bf62-4d62-9cb9-1b7230d23d5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "236cb548-d22e-40dd-986b-8bed91ae1777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8384e10a-bf62-4d62-9cb9-1b7230d23d5b",
                    "LayerId": "17b54054-d009-49dc-9cc6-acdc24c1b117"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "17b54054-d009-49dc-9cc6-acdc24c1b117",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8d4e3c64-2d65-4468-9d9f-b9295b1c692c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}