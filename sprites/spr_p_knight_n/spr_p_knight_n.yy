{
    "id": "490ac570-bc77-4324-b8c7-2f9428fafa1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_knight_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 4,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7aaa3dcc-9749-4c87-90c0-a2105821cbb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490ac570-bc77-4324-b8c7-2f9428fafa1a",
            "compositeImage": {
                "id": "2eac75ba-ab6e-407d-ba8d-7f4464efd782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aaa3dcc-9749-4c87-90c0-a2105821cbb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b921e79e-6a02-45a5-a673-404d7051bce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aaa3dcc-9749-4c87-90c0-a2105821cbb2",
                    "LayerId": "09449cc5-9a6a-4913-a468-bc14d2323189"
                }
            ]
        },
        {
            "id": "c5f07885-c914-49fa-a3a2-39c3e8791455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "490ac570-bc77-4324-b8c7-2f9428fafa1a",
            "compositeImage": {
                "id": "c5ec83c2-ad50-4506-aad6-772c9893ca66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5f07885-c914-49fa-a3a2-39c3e8791455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b0523ab-714f-492a-85e5-25bafb88c43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5f07885-c914-49fa-a3a2-39c3e8791455",
                    "LayerId": "09449cc5-9a6a-4913-a468-bc14d2323189"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "09449cc5-9a6a-4913-a468-bc14d2323189",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "490ac570-bc77-4324-b8c7-2f9428fafa1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}