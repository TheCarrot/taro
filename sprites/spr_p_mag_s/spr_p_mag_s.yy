{
    "id": "acba1337-0ea9-49e7-bb25-79641f57baec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_mag_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e35bf6e-273e-43c6-8958-b9ff2b4188c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acba1337-0ea9-49e7-bb25-79641f57baec",
            "compositeImage": {
                "id": "7c3c5eee-4296-4dbc-bc60-27f228df4e9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e35bf6e-273e-43c6-8958-b9ff2b4188c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f5871d-3b61-49fd-82cc-797bb71df7dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e35bf6e-273e-43c6-8958-b9ff2b4188c1",
                    "LayerId": "eb25f7d6-5670-4ff9-b52c-831e84d8f328"
                }
            ]
        },
        {
            "id": "d0de8f22-0c2d-4040-aafb-327995b5c1af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acba1337-0ea9-49e7-bb25-79641f57baec",
            "compositeImage": {
                "id": "7ad9d014-e276-4524-a13b-1f528714babc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0de8f22-0c2d-4040-aafb-327995b5c1af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eebac725-b585-4b3b-b7dd-b3150a219424",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0de8f22-0c2d-4040-aafb-327995b5c1af",
                    "LayerId": "eb25f7d6-5670-4ff9-b52c-831e84d8f328"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "eb25f7d6-5670-4ff9-b52c-831e84d8f328",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acba1337-0ea9-49e7-bb25-79641f57baec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}