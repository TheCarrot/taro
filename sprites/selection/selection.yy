{
    "id": "c5042fe7-cbca-4b59-9adf-737f3089d22f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "selection",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e75527a-2e3e-435a-a966-253245da0c23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5042fe7-cbca-4b59-9adf-737f3089d22f",
            "compositeImage": {
                "id": "09c6c131-f06e-4f5d-8525-b305d0c85c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e75527a-2e3e-435a-a966-253245da0c23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dec0bc2-52dd-4fe5-ac30-733e06932433",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e75527a-2e3e-435a-a966-253245da0c23",
                    "LayerId": "35eeb85b-9255-415a-a8c2-35c74fa1c276"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "35eeb85b-9255-415a-a8c2-35c74fa1c276",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5042fe7-cbca-4b59-9adf-737f3089d22f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}