{
    "id": "aeb9f2e6-19b5-445e-98bc-2c6d38556b4f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_priest_e",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 21,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98c23be1-ac9b-4cf6-876b-01d03272314e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb9f2e6-19b5-445e-98bc-2c6d38556b4f",
            "compositeImage": {
                "id": "d073bb94-c4cf-4925-92b9-650ae3082415",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c23be1-ac9b-4cf6-876b-01d03272314e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc273365-3406-47b4-b53f-eee43068780b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c23be1-ac9b-4cf6-876b-01d03272314e",
                    "LayerId": "d0251d5a-891d-4fbc-a8f7-d0d0cee43e00"
                }
            ]
        },
        {
            "id": "0f310442-a375-4f9f-9832-4e921b970bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aeb9f2e6-19b5-445e-98bc-2c6d38556b4f",
            "compositeImage": {
                "id": "a87beca4-9a4f-48b2-9471-24f3f6dfd60d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f310442-a375-4f9f-9832-4e921b970bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcd255dc-a8e8-41b9-8c16-4e16325f7ed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f310442-a375-4f9f-9832-4e921b970bed",
                    "LayerId": "d0251d5a-891d-4fbc-a8f7-d0d0cee43e00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d0251d5a-891d-4fbc-a8f7-d0d0cee43e00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aeb9f2e6-19b5-445e-98bc-2c6d38556b4f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}