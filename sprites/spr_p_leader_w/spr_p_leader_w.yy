{
    "id": "b9bb55ae-b032-4d8a-a95e-8db049620f22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_leader_w",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 10,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50257751-3a86-4dea-9013-e4910f9e9862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9bb55ae-b032-4d8a-a95e-8db049620f22",
            "compositeImage": {
                "id": "fa1cc5b5-0cd9-40c5-81ec-88d5e7a7930c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50257751-3a86-4dea-9013-e4910f9e9862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ed3449f-b836-43d1-ab12-5ae648982b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50257751-3a86-4dea-9013-e4910f9e9862",
                    "LayerId": "dd20e1bf-9c0c-4c35-89fe-733b80d4d166"
                }
            ]
        },
        {
            "id": "39626411-c688-4e15-bb92-27842d37f02e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9bb55ae-b032-4d8a-a95e-8db049620f22",
            "compositeImage": {
                "id": "ddf72ea2-dba3-4c84-ae39-934b80d55c4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39626411-c688-4e15-bb92-27842d37f02e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68071881-85b8-4889-b5f0-3d31dbd836a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39626411-c688-4e15-bb92-27842d37f02e",
                    "LayerId": "dd20e1bf-9c0c-4c35-89fe-733b80d4d166"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dd20e1bf-9c0c-4c35-89fe-733b80d4d166",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9bb55ae-b032-4d8a-a95e-8db049620f22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}