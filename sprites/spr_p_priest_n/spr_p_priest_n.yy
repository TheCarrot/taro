{
    "id": "7d769c54-86e5-42cd-9b8a-c7489bb0c7f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_priest_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4655b465-daf3-448f-bb90-b3ff3d5dcff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d769c54-86e5-42cd-9b8a-c7489bb0c7f4",
            "compositeImage": {
                "id": "44a4add5-5cf8-4f6b-a84d-e378f0168b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4655b465-daf3-448f-bb90-b3ff3d5dcff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08fe6fc2-e506-4bba-a39d-cdbffe04bc3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4655b465-daf3-448f-bb90-b3ff3d5dcff3",
                    "LayerId": "bc6b870f-9533-48b8-8ea0-86885006ef5e"
                }
            ]
        },
        {
            "id": "47275c95-0456-4e49-90dd-219439914042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d769c54-86e5-42cd-9b8a-c7489bb0c7f4",
            "compositeImage": {
                "id": "f1530089-cf9f-4906-8652-d3b7f3272fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47275c95-0456-4e49-90dd-219439914042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3303dc74-aa52-4b15-b07c-ddf39d755bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47275c95-0456-4e49-90dd-219439914042",
                    "LayerId": "bc6b870f-9533-48b8-8ea0-86885006ef5e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bc6b870f-9533-48b8-8ea0-86885006ef5e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d769c54-86e5-42cd-9b8a-c7489bb0c7f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}