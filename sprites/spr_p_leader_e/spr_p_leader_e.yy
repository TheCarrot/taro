{
    "id": "984076a6-871c-4711-abf5-6835677b21b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_leader_e",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17e7d242-45de-41e3-8469-21ab64022614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "984076a6-871c-4711-abf5-6835677b21b7",
            "compositeImage": {
                "id": "019300ab-5ab9-49d4-8bb6-a8b81d0ecdd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17e7d242-45de-41e3-8469-21ab64022614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c297376c-48f4-4278-86b6-3df0016386ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17e7d242-45de-41e3-8469-21ab64022614",
                    "LayerId": "b140de21-8e66-4acb-a9a4-845d3e540153"
                }
            ]
        },
        {
            "id": "a8299dee-8e48-4396-9277-2aae947c6995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "984076a6-871c-4711-abf5-6835677b21b7",
            "compositeImage": {
                "id": "c0dc29f7-70c3-44e7-9cab-2d24019caf37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8299dee-8e48-4396-9277-2aae947c6995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1989a812-a1ed-4cc2-b34a-150c3a2f8709",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8299dee-8e48-4396-9277-2aae947c6995",
                    "LayerId": "b140de21-8e66-4acb-a9a4-845d3e540153"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b140de21-8e66-4acb-a9a4-845d3e540153",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "984076a6-871c-4711-abf5-6835677b21b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}