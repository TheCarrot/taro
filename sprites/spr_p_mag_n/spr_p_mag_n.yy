{
    "id": "9365b9fa-803b-4cbf-963f-9b6949204d41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_mag_n",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de94e434-7487-4686-9f50-dc2d1fd8dadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9365b9fa-803b-4cbf-963f-9b6949204d41",
            "compositeImage": {
                "id": "21462adc-6a86-4537-a3c1-1c589473578e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de94e434-7487-4686-9f50-dc2d1fd8dadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dafff97-596b-4703-bf39-d084d046dfce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de94e434-7487-4686-9f50-dc2d1fd8dadc",
                    "LayerId": "4b7ef395-dab7-4e57-8559-b6252160e005"
                }
            ]
        },
        {
            "id": "c40ab0cf-6e06-497a-8ca2-a45d437ead52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9365b9fa-803b-4cbf-963f-9b6949204d41",
            "compositeImage": {
                "id": "1b68b8d6-8a62-4887-8274-ea1616fafec1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c40ab0cf-6e06-497a-8ca2-a45d437ead52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645a7eaa-8688-4564-87ab-c9390c20e558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c40ab0cf-6e06-497a-8ca2-a45d437ead52",
                    "LayerId": "4b7ef395-dab7-4e57-8559-b6252160e005"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4b7ef395-dab7-4e57-8559-b6252160e005",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9365b9fa-803b-4cbf-963f-9b6949204d41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}