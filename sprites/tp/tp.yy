{
    "id": "613f1192-a38c-422c-920b-c8e5418ef23e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ded6db18-9176-40dd-acb2-e7977d442fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "613f1192-a38c-422c-920b-c8e5418ef23e",
            "compositeImage": {
                "id": "42e72447-0c9a-4c79-9547-effa188f881c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded6db18-9176-40dd-acb2-e7977d442fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8636397-f7e0-4256-acfc-6851de42c80a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded6db18-9176-40dd-acb2-e7977d442fe1",
                    "LayerId": "5ca979f6-f6d5-40f8-98b3-00d5c3231894"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5ca979f6-f6d5-40f8-98b3-00d5c3231894",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "613f1192-a38c-422c-920b-c8e5418ef23e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}