{
    "id": "6040e660-8b28-4e78-be62-2e1d358a5d24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_knight_w",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 25,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d743b94-240a-43f6-8dfa-0b6e12b1b7e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040e660-8b28-4e78-be62-2e1d358a5d24",
            "compositeImage": {
                "id": "47726c0e-813c-44ad-9205-bf39db9ed80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d743b94-240a-43f6-8dfa-0b6e12b1b7e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b950be-3868-4d20-a481-04c19697f231",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d743b94-240a-43f6-8dfa-0b6e12b1b7e8",
                    "LayerId": "181b6a3b-5168-447f-a3d7-e599c2e710a0"
                }
            ]
        },
        {
            "id": "64c37f0d-a687-49e6-8354-9cb3f3e679ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6040e660-8b28-4e78-be62-2e1d358a5d24",
            "compositeImage": {
                "id": "8dcf9483-ac06-498c-8bc5-0e92937231fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c37f0d-a687-49e6-8354-9cb3f3e679ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37153337-c5b0-4676-a7dd-93dca54c3700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c37f0d-a687-49e6-8354-9cb3f3e679ae",
                    "LayerId": "181b6a3b-5168-447f-a3d7-e599c2e710a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "181b6a3b-5168-447f-a3d7-e599c2e710a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6040e660-8b28-4e78-be62-2e1d358a5d24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}