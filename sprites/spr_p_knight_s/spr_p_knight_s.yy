{
    "id": "7f5c692a-bd24-4a5e-ab53-94c67791fd60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_p_knight_s",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95af8096-74f9-4e79-abad-c214f1f4341a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f5c692a-bd24-4a5e-ab53-94c67791fd60",
            "compositeImage": {
                "id": "06832a33-6e43-4d1a-8f3a-5e15c7f33158",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95af8096-74f9-4e79-abad-c214f1f4341a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f36d7cd-629f-4968-9792-29758603e1b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95af8096-74f9-4e79-abad-c214f1f4341a",
                    "LayerId": "84902a11-28f5-4b93-834a-1dbe25bae4d8"
                }
            ]
        },
        {
            "id": "15111c80-e677-4d22-a28c-169a073ef887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f5c692a-bd24-4a5e-ab53-94c67791fd60",
            "compositeImage": {
                "id": "94b75757-78e9-4942-aece-766a326b5433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15111c80-e677-4d22-a28c-169a073ef887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27c4e156-ccb9-45e7-a4d6-14af5c84385c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15111c80-e677-4d22-a28c-169a073ef887",
                    "LayerId": "84902a11-28f5-4b93-834a-1dbe25bae4d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84902a11-28f5-4b93-834a-1dbe25bae4d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f5c692a-bd24-4a5e-ab53-94c67791fd60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}