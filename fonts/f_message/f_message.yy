{
    "id": "0862bb6f-bdad-49b6-b2ad-d09a7a76035e",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_message",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "af5dbdae-4f8c-4640-bd75-3713ef38f778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0ee210f7-7680-4603-9ce6-a80ad2149346",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 46,
                "y": 58
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e88e712e-d3b7-40d2-a210-a93a6e14e558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 40,
                "y": 58
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e03147b5-a970-463d-949f-ae911978a7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 58
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ba9be967-d3c3-4004-837c-b46bf9295d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 24,
                "y": 58
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c6c43069-5557-465d-899d-271fe6f011b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 12,
                "y": 58
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "75b16428-dfa0-45db-a80a-3e61e3db3b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "38eea713-2aa1-466a-a80d-c1f94e796177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 119,
                "y": 44
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "10517c83-adbd-4519-9c29-6d83e484a8fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "7fdc283a-a55b-4bdc-b1c6-d8fa14533444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 107,
                "y": 44
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d162bac5-96f6-4cfa-a4ec-d863cca8d8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 51,
                "y": 58
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7f391c57-9ee3-4655-ae34-7c16ae4b3390",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e8f42de6-1ddb-4c82-8280-54494f3d4398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bf9bc5a5-3b8d-46f8-a1b3-e6c2701423b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 80,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "28739655-ac69-4708-87cb-12d6829ac9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 76,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1d86c01f-12b9-40ae-9c1b-102c59864ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 70,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9da71e59-b7c3-4217-8cbd-337a47fda928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 62,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ad4739ff-c19e-48d7-a06f-53774d0eb422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 12,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a83a05e5-a249-45fd-95b2-51820f310f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 48,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2745f118-8ab5-4bfc-a69d-feb08f42d5f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 40,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "42e02277-3561-476f-bbdb-9da3893cdd57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "381a8ebd-38ba-44f7-9a97-799b247ad787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 91,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a3f9f751-73d2-422b-a4dc-45516ac1ffbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 58
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "61fa9e91-64ae-481a-973b-8cf95676c60a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 58
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ec9b8d79-6b33-4207-9ab2-190dc20a5939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 58
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ff0ebea9-95a2-41b2-895e-0c4e2827343a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f75718b5-85f6-4b24-a4d7-eb279583c2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 7,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "362b670c-4806-4b86-91d7-2ad163921ec6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "66d19524-57c1-4f61-8c7a-9656f8c2dc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 72
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f81e3d43-d089-47b5-9a42-e6767d0491bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 72
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d630f9d3-1c22-4ac7-b243-5469da78752a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 101,
                "y": 72
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a51e2988-dcda-4abf-b040-8765498f5047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 93,
                "y": 72
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3241ebae-8fbd-48a6-b481-59c45cd4c0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 12,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 80,
                "y": 72
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4e815caf-2a76-45de-a0af-b7991f6e0336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 12,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 69,
                "y": 72
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4c5b9910-78b0-4782-9739-80db72f1000a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 60,
                "y": 72
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "776d3d7d-6fe2-406c-822c-cf1c2e315e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e40de86a-2dd8-408d-9da4-dfd8b6a9426c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 72
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "125a0d79-9638-453a-9935-bd698faa3701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 31,
                "y": 72
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "18270c34-c775-43f5-9b2b-c81ccc797079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 22,
                "y": 72
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fb0aa53a-a6c3-4630-ad1f-347c04ed36ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 72
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d2ed0c4c-f97d-49c9-a148-8212aea51d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f503de09-0529-445c-81dc-bf543478f96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 117,
                "y": 58
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "25548b92-0fe9-4234-bb5f-99226988e185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 58
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "adbaee9f-f47d-4eb0-b773-087e723a8b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 100,
                "y": 58
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "bbf72dd0-1cd2-4851-a498-467a3b344ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 58
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "b1e68c43-437e-4ada-9668-d6050107039a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 81,
                "y": 58
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "980def30-4290-4cba-8d0e-e68f84895f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 22,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "533989a8-2560-4060-8e9d-f34176692ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "365fd2f6-cf31-4316-b0f4-e2f78103435a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a40bb125-751a-4286-aa0a-845e86d831e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 16
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d9cdc186-d52b-4225-a61e-ca9df8fe2ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 16
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c59735f3-595d-41dd-888c-0f80a92ee327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 16
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "39c2e1b3-6f1f-4767-be94-2ec1d23920bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 35,
                "y": 16
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "acab3f64-48ab-4671-9173-dbf303177571",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 25,
                "y": 16
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ef18a6dc-41ce-4259-ba35-b53826fc8f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 15,
                "y": 16
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d7ba7f98-f954-43e5-a141-d97b9ec4b5ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 12,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 16
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "73c480fa-905d-486b-80b7-78f802eb3299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c922f7ba-0a55-438e-9080-d6476fddf6b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b9f211ee-fb1e-4e8a-a2a0-c94ce6522e0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 12,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "52a79acf-5673-4530-8a46-8c877c2268bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 63,
                "y": 16
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "b7c64b55-07fd-4729-a66f-8039367d24ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ee8c14d5-6d41-4ee8-8a0d-e8dfbdd64dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 69,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "06edaf9e-11b4-41b1-9e06-fe24d7deca02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 12,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "52deba29-1665-4f48-8a16-ebb79b47d237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 12,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "c43dfd4b-17cf-4009-998a-ee64f6b0b8c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6d2cb39f-d166-4b95-93e3-14a78f5bd459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "751c239c-45a0-420d-860f-b5d179cb9637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fd4a8f94-1fe7-4ce7-90ac-d52e42f82a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "39cbb937-7c47-47a6-b58e-a77ddca3090d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ac485ef2-698f-416e-8fb1-88dc76218542",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "33375ee0-2538-4ddb-aa13-370a1627b4d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "5b2e95c1-81dd-4dbb-9502-832fe6ba6dbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 16
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "18c58d97-2718-410b-b717-199d12e905ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 36,
                "y": 30
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e8e3533f-8b59-453f-b180-ba450668cc62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 87,
                "y": 16
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1d1d4dc7-45a2-41e6-8f77-f85ec553ebc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 12,
                "offset": -1,
                "shift": 2,
                "w": 3,
                "x": 110,
                "y": 30
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "13f0a7fb-db4c-423b-ab41-4e85f6aa19de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 102,
                "y": 30
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bdd19816-0517-497f-b7e3-04ce9a6265bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 12,
                "offset": 0,
                "shift": 2,
                "w": 2,
                "x": 98,
                "y": 30
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c05a1837-9515-47dd-afaf-6869d50ebb74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 12,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 87,
                "y": 30
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2d6b16e0-3e8e-431e-82cf-07bce10b8507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 30
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "215692de-e54f-4402-8c71-7b26a3c9b3eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 71,
                "y": 30
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0b5e17e9-2404-4a31-a93e-82ae96412de0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 63,
                "y": 30
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1d536dd5-a5f1-42aa-9b21-6d91807a59c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 55,
                "y": 30
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5591cd45-442b-476e-9183-0109e2f9a7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 49,
                "y": 30
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c5421163-4a6f-413f-86be-a07bf15e453a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 30
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "d71ec4cd-a522-4595-a0df-07e20cdaef20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 12,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 44,
                "y": 30
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "569654f0-2b04-4b60-9fa6-d76ea228906e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 28,
                "y": 30
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "2926036c-60e0-47d1-be5f-6944cf013a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 20,
                "y": 30
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0f9c0661-9e29-442e-a67c-caf252fc942d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 12,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 10,
                "y": 30
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2b5ef891-767c-41ac-a401-928468692701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "05fabc9f-9d45-49bc-95ff-1f8d415537e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 114,
                "y": 16
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "119d642e-f7c9-42cc-94a7-9235731c9584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 106,
                "y": 16
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e0fa5322-f3d9-4068-8ec3-c823bfcf0b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 100,
                "y": 16
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "005370d1-038c-4d09-8247-a474a7e0ae4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 12,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 97,
                "y": 16
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0926e91f-3bc8-43e2-b309-db50305c21df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 12,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 91,
                "y": 16
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "79507f74-63cc-4c3e-88f4-da18b3144d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 12,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4492022b-0c0f-49b8-b9e4-118f223b469d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 12,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 27,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "cc5c5ed8-0a64-4d31-a6fd-d94541bbb271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "fea63d4d-443c-4bb6-bf83-c8ae5c30ef45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "9ff8da47-6e8e-4fab-bd5f-49e03f74d77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "5e535c2c-8916-4bcb-b4d1-47146d77a236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "bcd4c227-10af-4d75-b57a-2b359305e2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2ea38273-6833-4af8-b9c7-add4edf1e0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "695ecb11-e450-4f1a-8ee3-0dea0b938918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "95b4cf6d-ef50-453f-aa54-64c5cc7fb171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "ccebf7e4-cafa-4d9b-98eb-93df5e355596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "591082ae-05f0-44a1-88a6-75403d6ada22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "647b72d0-6da2-4439-a646-ced7d7c1520b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "c62b567f-d379-4100-9b46-f96600af896e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "d8005986-8256-41f2-8f49-8228f8eb8f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "c0d8a58f-ae9a-4031-938b-d5350f0c162b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "ce4f75dd-397b-462b-9df6-bac9987f5e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "b53f0737-8334-4862-bda7-7e0e45f90887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 8,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}