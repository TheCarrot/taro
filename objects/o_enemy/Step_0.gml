if (!selected) image_blend = -1;
sprite_set_speed(sprite_index,2,sprite_get_speed_type(sprite_index));
if (!is_undefined(squad)) {
	if (squad.active_player == self && !selected) {
		global.camera.target = self;
		selected_overlay_timer = 200;
		s_o_t_direction = -1;
		selected = true;
	}
}	
if (selected) {
	image_blend = merge_color(make_color_rgb(255, 255, 255), make_color_rgb(250 * level / 10, 250 - 250 * level / 10, 0), 0.25);
}
else {
	image_blend = make_color_rgb(250 * level / 10, 250 - 250 * level / 10, 0);
}