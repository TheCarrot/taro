/// @description Insert description here
// You can write your code in this editor
if (!is_undefined(cooldown)) {
	cooldown -= show_speed * 0.1;
	if (cooldown <= 0)
		instance_destroy();
}
if (!is_undefined(holder) && holder.current == self && !visible)
	visible = true;
x = room_width / 2 - width / 2;
y = room_height - width - 32;