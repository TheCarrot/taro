{
    "id": "088ac052-de03-4985-99bf-0a316a63afe3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_message",
    "eventList": [
        {
            "id": "e8dfa3e4-c39a-47de-a72d-712fb85917e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "088ac052-de03-4985-99bf-0a316a63afe3"
        },
        {
            "id": "2f94df67-5f2a-480e-95cb-f145850835a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "088ac052-de03-4985-99bf-0a316a63afe3"
        },
        {
            "id": "ddbacda5-dd76-44cd-8a4a-5adaad3ec4a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "088ac052-de03-4985-99bf-0a316a63afe3"
        },
        {
            "id": "8cca9520-e12f-40be-9941-f0677caeaaf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "088ac052-de03-4985-99bf-0a316a63afe3"
        },
        {
            "id": "f2a5a782-7dd3-4016-8e95-2901c5bf7b4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "088ac052-de03-4985-99bf-0a316a63afe3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}