randomize();
squad[0] = undefined;
count = 0;
active_player = undefined;
active_player_num = -1;
if (!is_undefined(global.squad)) {
	count = irandom_range(global.squad.count + 1, global.squad.count * 2 + 1);
	var lvl = 1;
	for (var i = 0; i < global.squad.count; i++)
		lvl += global.squad.squad[i].level;
	lvl = ceil(lvl / global.squad.count);
	for (i = 0; i < count; i++) {
		squad[i] = instance_create_layer(x + TILE_SIZE * i, y, "iPlayer", o_enemy);
		squad[i].squad = self;
		set_member_role(squad[i]);
		switch (squad[i].role) {
		case 0: //knight
			squad[i].strength = 3;
			squad[i].damage_delta = 1;
			squad[i].defence = 2;
			squad[i].agility = 1;
			break;
		case 1: //priest
			squad[i].strength = 1;
			squad[i].damage_delta = 1;
			squad[i].defence = 1;
			squad[i].agility = 1;
			break;
		case 2: //magician
			squad[i].strength = 1;
			squad[i].damage_delta = 2;
			squad[i].defence = 1;
			squad[i].agility = 1;
			break;
		case 3: //leader
			squad[i].strength = 2;
			squad[i].damage_delta = 1;
			squad[i].defence = 1;
			squad[i].agility = 3;
			break;
		}
		squad[i].skin = global.player_skin[squad[i].role];
		squad[i].sprite_index = squad[i].skin[0];
		lvl_up(squad[i], irandom_range(lvl - 1, lvl + 1), false);
	}
}