{
    "id": "4a1b96ba-7c9a-46d8-b976-5d12ca9fb315",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player_squad",
    "eventList": [
        {
            "id": "7a5fa2ba-e6ba-43fd-b35b-f6486b15a360",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a1b96ba-7c9a-46d8-b976-5d12ca9fb315"
        },
        {
            "id": "8937b532-6748-4af8-909d-fb1a3e5ce537",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4a1b96ba-7c9a-46d8-b976-5d12ca9fb315"
        },
        {
            "id": "14472918-735a-489c-a64e-6996fdd453dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 69,
            "eventtype": 9,
            "m_owner": "4a1b96ba-7c9a-46d8-b976-5d12ca9fb315"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}