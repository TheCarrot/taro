#region Skins
skin_priest[0] = spr_p_priest_s;
skin_priest[1] = spr_p_priest_n;
skin_priest[2] = spr_p_priest_e;
skin_priest[3] = spr_p_priest_w;

skin_knight[0] = spr_p_knight_s;
skin_knight[1] = spr_p_knight_n;
skin_knight[2] = spr_p_knight_e;
skin_knight[3] = spr_p_knight_w;

skin_magician[0] = spr_p_mag_s;
skin_magician[1] = spr_p_mag_n;
skin_magician[2] = spr_p_mag_e;
skin_magician[3] = spr_p_mag_w;

skin_leader[0] = spr_p_leader_s;
skin_leader[1] = spr_p_leader_n;
skin_leader[2] = spr_p_leader_e;
skin_leader[3] = spr_p_leader_w;

global.player_skin[0] = skin_knight;
global.player_skin[1] = skin_priest;
global.player_skin[2] = skin_magician;
global.player_skin[3] = skin_leader;
#endregion

#region Init
move_together = true;

squad[0] = undefined;
count = 3;
active_player = squad[0];
active_player_num = 0;

for (var i = 0; i < count; i++) {
	squad[i] = instance_create_depth(x,y,depth,o_player);
	squad[i].visible = true;
	squad[i].squad = self;
	squad[i].role = set_member_role(squad[i]);
	switch (squad[i].role) {
		case 0: //knight
			squad[i].strength = 3;
			squad[i].damage_delta = 1;
			squad[i].defence = 2;
			squad[i].agility = 1;
			break;
		case 1: //priest
			squad[i].strength = 1;
			squad[i].damage_delta = 1;
			squad[i].defence = 1;
			squad[i].agility = 1;
			break;
		case 2: //magician
			squad[i].strength = 1;
			squad[i].damage_delta = 2;
			squad[i].defence = 1;
			squad[i].agility = 1;
			break;
		case 3: //leader
			squad[i].strength = 2;
			squad[i].damage_delta = 1;
			squad[i].defence = 1;
			squad[i].agility = 3;
			break;
	}
	squad[i].skin = global.player_skin[squad[i].role];
	squad[i].sprite_index = squad[i].skin[0];
	if (i > 0) squad[i].guide = squad[i-1];
	else squad[i].guide = squad[array_length_1d(squad) - 1];
}
active_player = squad[0];
global.squad = self;
instance_create_layer(x, y, "iPlayer", o_camera);
#endregion