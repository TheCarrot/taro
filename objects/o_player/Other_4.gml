lay_id = layer_get_id("tCollision");
map_id = layer_tilemap_get_id(lay_id);
if (!is_undefined(squad) && squad.move_together) {
	squad.active_player.x = squad.x;
	squad.active_player.y = squad.y;
	for (var i = 0; i < array_length_1d(squad.squad); i++) {
		squad.squad[i].x = squad.active_player.x;
		squad.squad[i].y = squad.active_player.y;
		squad.squad[i].toy = squad.squad[i].y;
		squad.squad[i].tox = squad.squad[i].x;
	}
}