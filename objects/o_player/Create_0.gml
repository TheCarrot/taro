#region Main Variables

step_speed = 2;
image_speed = 2;

can_move = true;
selected = true;
tox = x;
toy = y;
prev_x = x;
prev_y = y;
squad = undefined;
guide = undefined;
skin = global.player_skin[0];

hp = 10;
level = 1;
strength = 1;
defence = 1;
damage_delta = 1;
agility = 1;
ability = undefined;
ability_lvl = 1;
role = undefined;

#endregion