#region Movement
if (moveable()) {
	prev_prev_x = prev_x;
	prev_prev_y = prev_y;
	selected = squad != undefined && squad.active_player == self || squad == undefined;
	if (selected) global.camera.target = self;
	if (!selected && squad.move_together && can_move && sqrt((guide.x - x) * (guide.x - x) + (guide.y - y) * (guide.y - y)) > TILE_SIZE) {
		prev_x = x;
		prev_y = y;
		tox = guide.prev_x;
		toy = guide.prev_y;
		can_move = false;
	}
	if (selected && can_move) {
		if (global.BUP) {
			prev_x = x;
			prev_y = y;
			toy = y - TILE_SIZE;
			can_move = false;
		}
		else if (global.BDOWN) {
			prev_x = x;
			prev_y = y;
			toy = y + TILE_SIZE;
			can_move = false;
		}
		else if (global.BLEFT) {
			prev_x = x;
			prev_y = y;
			tox = x - TILE_SIZE;
			can_move = false;
		}
		else if (global.BRIGHT) {
			prev_x = x;
			prev_y = y;
			tox = x + TILE_SIZE;
			can_move = false;
		}
	}
	
	if (tox < 0 || toy < 0 || tox > room_width - 1 || toy > room_height - 1) {
		tox = x; toy = y;
		prev_x = prev_prev_x;
		prev_y = prev_prev_y;
	}
	
	if (can_move == false) {
		var tcx = floor(tox / TILE_SIZE);
		var tcy = floor(toy / TILE_SIZE);
		var tile = tilemap_get(map_id, tcx, tcy);
		if (tile > 0) {
			tox = x;
			toy = y;
			prev_x = prev_prev_x;
			prev_y = prev_prev_y;
		}
	}
	
	x += sign(tox - x) * step_speed;
	y += sign(toy - y) * step_speed;
	
	if (tox != x) { 
		sprite_index = tox < x ? skin[3] : skin[2];
		sprite_set_speed(sprite_index, 3, sprite_get_speed_type(sprite_index));
	}
	else if (toy != y) {
		sprite_index = toy < y ? skin[1] : skin[0];
		sprite_set_speed(sprite_index, 3, sprite_get_speed_type(sprite_index));
	}
	else {
		can_move = true;
		sprite_set_speed(sprite_index, 1, sprite_get_speed_type(sprite_index));
	}
}
#endregion