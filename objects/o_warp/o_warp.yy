{
    "id": "6341d0f3-fde0-4c1a-9ffd-8f90a04f8601",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_warp",
    "eventList": [
        {
            "id": "767d6367-5d0b-4a9d-b071-22b6abd7d487",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6341d0f3-fde0-4c1a-9ffd-8f90a04f8601"
        },
        {
            "id": "7578beb2-0cf6-4da1-b274-8faab8edb56a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0c246e2d-239f-4973-ab06-d3b42a27e826",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6341d0f3-fde0-4c1a-9ffd-8f90a04f8601"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "613f1192-a38c-422c-920b-c8e5418ef23e",
    "visible": true
}