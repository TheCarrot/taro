if (global.player_can_warp == true && other.selected) {
	if (room == r_dungeon) {
		if (other.squad != undefined)
			other.squad.move_together = true;
		room_goto(r_world);
	}
	else if (room == r_world) {
		if (other.squad != undefined)
			other.squad.move_together = false;
		room_goto(r_dungeon);
	}
}