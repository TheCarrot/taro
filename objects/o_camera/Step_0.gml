#region Selection

if (global.BBACK && global.battle) {
	if (select) {
		instance_destroy(o_select);
	}
	else {
		instance_create_layer(round(target.x / TILE_SIZE) * TILE_SIZE, 
		round(target.y / TILE_SIZE) * TILE_SIZE, "iInstances", o_select);
		target = o_select;
	}
	select = !select;
}

if (global.BSEL && global.battle && select) {
	for (var cn = 0; cn < array_length_1d(global.squad.squad); cn++) {
		if (target.x == global.squad.squad[cn].x && target.y == global.squad.squad[cn].y) {
			o_player_squad.active_player_num = cn;
			o_player_squad.active_player = o_player_squad.squad[cn];
			break;
		}
	}
	select = 0;
	instance_destroy(o_select);
}

if (!select) { /*
	if (!is_undefined(global.squad))
		target = global.squad.active_player;
	else
		target = o_player; */
	if (!is_undefined(global.battle_controller)) {
		if (global.battle_controller.enemy_turn)
			target = global.battle_controller.enemy_squad.active_player;
		else
			target = global.squad.active_player;
	}
	else
		if (!is_undefined(global.squad))
			target = global.squad.active_player;
		else target = o_player;
}

#endregion


width = camera_get_view_width(view_camera[0]);
height = camera_get_view_height(view_camera[0]);
zoomx = room_width/width;
zoomy = room_height/height;

if (is_undefined(target)) exit;
x = lerp(x, target.x, .1);
y = lerp(y, target.y, .1);

//x = clamp(x, width/zoomx + CELL_WIDTH, room_width-width/zoomx - CELL_WIDTH);
//y = clamp(y, height/zoomy + CELL_HEIGHT, room_height-height/zoomy - CELL_HEIGHT);
camera_set_view_pos(view_camera[0], x-width/zoomx, y-height/zoomy);