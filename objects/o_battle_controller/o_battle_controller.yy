{
    "id": "149a8e32-00df-4aac-8f10-608ff3251ae5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_battle_controller",
    "eventList": [
        {
            "id": "99dc0928-6047-44af-b223-f67c44fad559",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "149a8e32-00df-4aac-8f10-608ff3251ae5"
        },
        {
            "id": "f066175e-e365-4274-a639-b9c415ee972b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "149a8e32-00df-4aac-8f10-608ff3251ae5"
        },
        {
            "id": "b452e639-b09f-4010-8ea6-da8bb7d8d80f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "149a8e32-00df-4aac-8f10-608ff3251ae5"
        },
        {
            "id": "8dd8adb7-83a3-4924-9c1a-17eacab7121f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 9,
            "m_owner": "149a8e32-00df-4aac-8f10-608ff3251ae5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}