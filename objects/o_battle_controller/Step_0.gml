if (enemy_turn) {
	if (!is_undefined(global.squad.active_player))
		global.squad.active_player.selected = false;
	global.squad.active_player = undefined;
	if (is_undefined(enemy_squad.active_player)) {
		if (enemy_cur_pointer >= enemy_squad.count || enemy_cur_pointer < 0) {
			enemy_queue = array_shuffle(enemy_squad.squad, enemy_squad.count);
			enemy_cur_pointer = 0;
		}
		enemy_squad.active_player = enemy_queue[enemy_cur_pointer++];
	}
	enemy_squad.active_player.selected = true;;
}
else {
	if (!is_undefined(enemy_squad.active_player)) 
		enemy_squad.active_player.selected = false;
	enemy_squad.active_player = undefined;
	if (is_undefined(global.squad.active_player)) {
		if (player_cur_pointer >= global.squad.count || player_cur_pointer < 0) {	
			player_queue = array_shuffle(global.squad.squad, global.squad.count);
			player_cur_pointer = 0;
		}
		global.squad.active_player = player_queue[player_cur_pointer++];
	}
	global.squad.active_player.selected = true;
}