randomize();
global.battle_controller = self;
global.battle = true;
enemy_turn = random(10) > 5;
enemy_squad = instance_create_depth(x,y,depth,o_enemy_squad);

enemy_queue = array_shuffle(enemy_squad.squad, enemy_squad.count);
player_queue = array_shuffle(global.squad.squad, global.squad.count);
enemy_cur_pointer = 0;
player_cur_pointer = 0;