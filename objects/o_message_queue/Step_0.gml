if (!ds_queue_empty(queue)) {
	if (is_undefined(current)) {
		current = ds_queue_dequeue(queue);
		current.holder = self;
		//current.visible = true;
	}
	if (!wants_to_die) wants_to_die = true;
}
if (is_undefined(current) && ds_queue_empty(queue) && wants_to_die)
	instance_destroy();