{
    "id": "dccce062-89e8-4aae-a569-f8f1cf87b835",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_level",
    "eventList": [
        {
            "id": "5e63b2b3-2666-4c5d-8fee-546c1ebe274a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dccce062-89e8-4aae-a569-f8f1cf87b835"
        },
        {
            "id": "96af25bd-eec7-43b1-b5e3-d4b1c3875ab9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "dccce062-89e8-4aae-a569-f8f1cf87b835"
        },
        {
            "id": "860013c3-821f-46fe-b0a7-b5de76cabdec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "dccce062-89e8-4aae-a569-f8f1cf87b835"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}