randomize()

#region Constants

var wall_map_id = layer_tilemap_get_id("tCollision");
width = room_width div CELL_WIDTH;
height = room_height div CELL_HEIGHT;
grid = ds_grid_create(width, height);
ds_grid_set_region(grid, 0, 0, width, height, VOID);

var crx = width div 2, cry = height div 2;
var dir = irandom(3), steps = 700;
var dir_change_prob = 1; //50%
var free_cells = 0;
var playerspawn = 0;
var enemyspawn = 0;
var exitspawn = 0;
var cnt = 0;

#endregion

#region Generating and Filling

repeat (steps) {
	grid[# crx, cry] = FLOOR;
	
	if (irandom(dir_change_prob) == dir_change_prob)
		dir = irandom(3);
	
	var x_dir = lengthdir_x(1, dir * 90), 
		y_dir = lengthdir_y(1, dir * 90);
	crx += x_dir;
	cry += y_dir;
	if (crx < 2 || crx >= width - 2)
		crx += -x_dir * 2;
	if (cry < 2 || cry >= height - 2)
		cry += -y_dir * 2;
}

for (var _y = 1; _y < height-1; _y++) {
	for (var _x = 1; _x < width-1; _x++) {
		if (grid[# _x, _y] != FLOOR) {
			var ut = grid[# _x, _y-1] == VOID,
				lt = grid[# _x-1, _y] == VOID,
				rt = grid[# _x+1, _y] == VOID,
				dt = grid[# _x, _y+1] == VOID;
			var tind = UP*ut + LEFT*lt + RIGHT*rt + DOWN*dt + 1;
			if (tind == 1)
				grid[# _x, _y] = FLOOR;
		}
		else free_cells++;
	}
}

for (var _y = 1; _y < height-1; _y++) {
	for (var _x = 1; _x < width-1; _x++) {
		if (grid[# _x, _y] != FLOOR) {
			var ut = grid[# _x, _y-1] == VOID,
				lt = grid[# _x-1, _y] == VOID,
				rt = grid[# _x+1, _y] == VOID,
				dt = grid[# _x, _y+1] == VOID;
			var tind = UP*ut + LEFT*lt + RIGHT*rt + DOWN*dt + 1;
			tilemap_set(wall_map_id, tind, _x, _y);
		}
		else {
			cnt++;
			if (playerspawn < 1) {
				if (!is_undefined(global.squad)) {
					var q = ds_queue_create();
					var used = ds_map_create();
					var cn = 0;
					ds_map_add(used, string(_x) + "," + string(_y), VOID);
					set_player(cn, _x, _y);
					ds_queue_enqueue(q, [_x, _y]);
					++cn;
					while(cn < array_length_1d(global.squad.squad) && !ds_queue_empty(q)) {
						var curr = ds_queue_dequeue(q);
						for (var dir = 0; dir < 4; ++dir) {
							var nx = curr[0] + lengthdir_x(1, dir * 90), ny = curr[1] + lengthdir_y(1, dir * 90);
							if (is_undefined(ds_map_find_value(used, string(nx) + "," + string(ny))) && grid[# nx, ny] == FLOOR) {
								ds_map_add(used, string(nx) + "," + string(ny), VOID);
								if (cn < array_length_1d(global.squad.squad)) {
									set_player(cn, nx, ny);
								}
								ds_queue_enqueue(q, [nx, ny]);
								++cn;
							}
						}
					}
					ds_queue_destroy(q);
					ds_map_destroy(used);
					playerspawn = 1;
				}
			}
			else if (enemyspawn < 1) {
				instance_create_layer(_x * TILE_SIZE, _y * TILE_SIZE, "iPlayer", o_battle_controller);
				enemyspawn = 1;
			}
			if (cnt == free_cells) {
				instance_create_layer(_x * TILE_SIZE, _y * TILE_SIZE, "iPlayer", o_warp);
				exitspawn = 1;
			}
		}
	}
}

#endregion