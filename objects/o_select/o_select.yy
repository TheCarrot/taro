{
    "id": "bdc435c3-c249-4caa-9cfb-3c47d374b898",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_select",
    "eventList": [
        {
            "id": "1cb4be5a-91ee-4ef2-a52e-faf73afee2dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdc435c3-c249-4caa-9cfb-3c47d374b898"
        },
        {
            "id": "9062fdc6-a108-45f1-b2f0-850babc88499",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdc435c3-c249-4caa-9cfb-3c47d374b898"
        },
        {
            "id": "ca5a7bc9-c51d-4fad-85e3-2b8c35834daa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1d285fc3-339b-4c75-94f8-9a77bac46391",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bdc435c3-c249-4caa-9cfb-3c47d374b898"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c5042fe7-cbca-4b59-9adf-737f3089d22f",
    "visible": true
}