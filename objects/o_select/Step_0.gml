if (o_camera.select) {
	if (global.BUP && toy == y) {
		toy = y - TILE_SIZE;
	}
	if (global.BDOWN && toy == y) {
		toy = y + TILE_SIZE;
	}
	if (global.BLEFT && tox == x) {
		tox = x - TILE_SIZE;
	}
	if (global.BRIGHT && tox == x) {
		tox = x + TILE_SIZE;
	}
}

if (tox < 31 || toy < 31 || tox > room_width - 63 || toy > room_height - 63) {
	tox = x; toy = y;
}

x += sign(tox - x) * step_speed;
y += sign(toy - y) * step_speed;