#region Hold button

if (keyboard_check(vk_up) || keyboard_check(ord("W")))
	global.BUP = true;
else
	global.BUP = false;
	
if (keyboard_check(vk_left) || keyboard_check(ord("A")))
	global.BLEFT = true;
else
	global.BLEFT = false;
	
if (keyboard_check(vk_down) || keyboard_check(ord("S")))
	global.BDOWN = true;
else
	global.BDOWN = false;
	
if (keyboard_check(vk_right) || keyboard_check(ord("D")))
	global.BRIGHT = true;
else
	global.BRIGHT = false;

#endregion

#region Press button

if (keyboard_check_pressed(vk_shift) || keyboard_check_pressed(ord("X")))
	global.BBACK = true;
else
	global.BBACK = false;
	
if (keyboard_check_pressed(vk_enter) || keyboard_check_pressed(ord("Z")))
	global.BSEL = true;
else
	global.BSEL = false;

#endregion