#region Controls variables

global.BUP = false;
global.BLEFT = false;
global.BDOWN = false;
global.BRIGHT = false;
global.BBACK = false;
global.BSEL = false;
global.PU = false;
global.PL = false;
global.PD = false;
global.PR = false;
global.PBACK = false;
global.PSELECT = false;

#endregion

global.player_can_warp = true;
global.player_warpx = 0;
global.player_warpy = 0;
global.battle_controller = undefined;
global.battle = false;

room_goto(r_world);
game_set_speed(60, fps);