#macro TILE_SIZE 32

#macro CELL_WIDTH 32
#macro CELL_HEIGHT 32
#macro FLOOR -5
#macro WALL -6
#macro VOID -7

#macro UP 1
#macro LEFT 2
#macro RIGHT 4
#macro DOWN 8 