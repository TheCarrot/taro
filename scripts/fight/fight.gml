// argument0 - attacking character, argument1 - defensive character, argument2 - damage type

randomize();
var block = random(15);
block = block < argument1.agility / 2 || block < argument1.defence / 2;
var back_hit = random(20) > 20 - argument1.agility;
var double_hit = random(10) < argument0.agility / 2;
var crit = random(15);
crit = crit < argument0.agility / 2 || crit < argument0.strength / 2;

var role_names;
role_names[0] = "Knight ";
role_names[1] = "Priest ";
role_names[2] = "Magician ";
role_names[3] = "Leader ";
var msg_which = argument0.object_index == o_player ? "Player's " : "Enemy's ";
var msg_other = argument0.object_index == o_player ? "enemy's " : "player's ";
var msg_queue = instance_create_depth(x,y,depth,o_message_queue);

var damage_val = argument2 == 0 ? argument0.strength : argument0.ability;
var damage = damage_val * damage_val / argument1.defence;
if (!block) {
	var repeats = 1;
	if (double_hit) repeats++;
	for(;repeats > 0; repeats--) {
		if (double_hit && repeats == 1) {
			crit = random(15);
			crit = crit < argument0.agility / 2 || crit < argument0.strength / 2;
			var ddmsg = instance_create_depth(x,y,depth,o_message);
			ddmsg.cooldown = speed * 3;
			ddmsg.text = role_names[argument0.role] + "attacks again!"
			ddmsg.visible = false;
			ds_queue_enqueue(msg_queue.queue, ddmsg);
		}
		else {
			var msg = instance_create_depth(x,y,depth,o_message);
			msg.cooldown = speed * 3;
			msg.text = msg_which + role_names[argument0.role] + "attacks " + msg_other + role_names[argument1.role] + "!"; 
			msg.visible = false;
			ds_queue_enqueue(msg_queue.queue, msg);
			
		}
		if (crit) {
			var cmsg = instance_create_depth(x,y,depth,o_message);
			cmsg.cooldown = speed * 3;
			cmsg.text = "CRITICAL DAMAGE!"
			cmsg.visible = false;
			ds_queue_enqueue(msg_queue.queue, cmsg);
			crit = false;
		}
		var smsg = instance_create_depth(x,y,depth,o_message);
		smsg.cooldown = speed * 3;
		smsg.text = role_names[argument0.role] + "hits " + string(crit ? damage * 1.5 : damage) + " damage.";
		smsg.visible = false;
		ds_queue_enqueue(msg_queue.queue, smsg);
		
		argument1.hp -= crit ? damage * 1.5 : damage;
		var prevLevel = argument0.level;
		var modificator = crit ? 0.25 : 0.2;
		argument0.level += modificator * argument1.level / prevLevel;
	}
}
else {
	var bmsg = instance_create_depth(x,y,depth,o_message);
	bmsg.cooldown = speed * 3;
	bmsg.text = role_names[argument1.role] + "blocks!";
	bmsg.visible = false;
	ds_queue_enqueue(msg_queue.queue, bmsg);
}
if (back_hit) {
	fight(argument1, argument0, 0);
}