///Shuffling array function

//argument0: the array to shuffle
//argument1: the size of the array

var i, j;
var arr = array_create(argument1);
array_copy(arr, 0, argument0, 0, argument1);

for (i = 0; i < argument1; i++) {
    j = irandom_range(i, argument1 - 1);
    if (i != j) {
        k = arr[i];
        arr[i] = arr[j];
        arr[j] = k;
    }
}

return arr;