// argument0 - member
if (is_undefined(argument0.squad)) {
	argument0.role = 0;
	return;
}

var role_arr;
role_arr[0] = 0; //knight
role_arr[1] = 0; //priest
role_arr[2] = 0; //magician
role_arr[3] = 0; //leader
for (var i = 0; i < array_length_1d(argument0.squad.squad); i++)
	if (argument0.squad.squad[i].role != undefined)
		role_arr[argument0.squad.squad[i].role]++;

randomize();
if (random(10) > 5 && role_arr[3] == 0 && (role_arr[0] > 0 || argument0.squad.count >= 5))
	argument0.role = 3;
else argument0.role = irandom_range(0,2);
if (argument0.role == 1 && role_arr[1] + 1 > argument0.squad.count / 3)
	argument0.role = irandom_range(0,1) * 2;
return argument0.role;