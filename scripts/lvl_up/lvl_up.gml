// argument0 - object to lvlup, argument1 - lelel to up to, argument2 - bool show lvlup scene
var diff_arr;
diff_arr[0] = argument0.hp;
diff_arr[1] = argument0.strength;
diff_arr[2] = argument0.defence;
diff_arr[3] = argument0.agility;
diff_arr[4] = argument0.ability_lvl;
for(; argument0.level < argument1; argument0.level++){
	randomize();
	switch (argument0.role) {
		case 0: //knight
			argument0.hp += irandom_range(0, argument0.hp / 3);
			++argument0.strength;
			++argument0.defence;
			argument0.agility += irandom_range(1,3) / 2;
			if (argument0.level % 3 == 0 && argument0.level < 10)
				argument0.ability_lvl++;
			break;
		case 1: //priest
			argument0.hp += irandom_range(1, argument0.hp / 4);
			argument0.strength += 0.5;
			argument0.defence += 0.34;
			argument0.agility += 0.5 * random_range(1,2.5);
			if (argument0.level % 3 == 0 && argument0.level < 10)
				argument0.ability_lvl++;
			break;
		case 2: //magician
			argument0.hp += irandom_range(0, argument0.hp / 5);
			argument0.strength += 0.2;
			argument0.defence += 0.25;
			argument0.agility += irandom_range(0,1) / 2;
			if (argument0.level % 3 == 0 && argument0.level < 10)
				argument0.ability_lvl++;
			break;
		case 3: //leader
			argument0.hp += irandom_range(0, argument0.hp / 4);
			argument0.strength += irandom_range(1,4) / 2;
			argument0.defence += 0.5;
			argument0.agility += irandom_range(1,3) / 2;
			if (argument0.level % 3 == 0 && argument0.level < 10)
				argument0.ability_lvl++;
			break;
	}
}
if (is_bool(argument2) && argument2) {
	diff_arr[0] = floor(argument0.hp - diff_arr[0]);
	diff_arr[1] = floor(argument0.strength - diff_arr[1]);
	diff_arr[2] = floor(argument0.defence - diff_arr[2]);
	diff_arr[3] = floor(argument0.agility - diff_arr[3]);
	diff_arr[4] = floor(argument0.ability_lvl - diff_arr[4]);
	// show lvlup scene for each non-zero diff_arr valued stat
}